package io.debezium.relational;

public class ProcedureId {
    private final String catalogName;
    private final String schemaName;
    private final String procedureName;

    public ProcedureId(String catalogName, String schemaName, String procedureName) {
        this.catalogName = catalogName;
        this.schemaName = schemaName;
        this.procedureName = procedureName;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getProcedureName() {
        return procedureName;
    }
}
