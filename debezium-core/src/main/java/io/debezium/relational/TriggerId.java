package io.debezium.relational;

public class TriggerId {
    private final String catalogName;
    private final String schemaName;
    private final String triggerName;

    public TriggerId(String catalogName, String schemaName, String triggerName) {
        this.catalogName = catalogName;
        this.schemaName = schemaName;
        this.triggerName = triggerName;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getTriggerName() {
        return triggerName;
    }
}
