package io.debezium.relational;

public class FunctionId {
    private final String catalogName;
    private final String schemaName;
    private final String functionName;

    public FunctionId(String catalogName, String schemaName, String functionName) {
        this.catalogName = catalogName;
        this.schemaName = schemaName;
        this.functionName = functionName;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public String getFunctionName() {
        return functionName;
    }
}
